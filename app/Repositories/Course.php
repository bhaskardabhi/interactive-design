<?php declare(strict_types=1);

namespace App\Repositories;

use Illuminate\Database\Eloquent\Collection;
use App\User;
use App\Quiz;
use App\QuizAnswer;
use App\Course as CourseModel;

class Course
{
    public function getUserLeaderboard($id, $userId, array $filter = []){
        $all = QuizAnswer::with('user')
            ->selectRaw('user_id,SUM(score) as total_score, user_id = '.$userId.' as is_current')
            ->byCourse($id)
            ->when(isset($filter['country_id']) && $filter['country_id'] !== null, function ($query) use ($filter) { 
                return $query->byCountryUser($filter['country_id']);
            })
            ->groupBy('user_id')
            ->orderByRaw('SUM(score) DESC, is_current DESC')
            ->get();

        $top = $all->slice(0, 3)->map(function ($item, $index){
            $item->rank = $index + 1;
            return $item;
        });

        $bottom = $all->slice($all->count() - 3, 3)->map(function ($item, $index){
            $item->rank = $index + 1;
            return $item;
        });

        $surrounding = $all->slice($all->search(function ($item, $key) use ($userId) {
            return $item->user_id == $userId;
        })-1, 3)->map(function ($item, $index){
            $item->rank = $index + 1;
            return $item;
        });

        $users = collect($top)->merge($bottom)->merge($surrounding)
                ->unique('user_id')
                ->sortBy('rank')
                ->values()
                ->toArray();

        return [
            'user' => collect($users)->filter(function ($user) use ($userId) {
                return $user['user_id'] == $userId;
            })->first(),
            'participants' => $users
        ];
    }

    public function getUserLeaderboardBySql($id, $userId, array $filter = []){
        $countryQuery = isset($filter['country_id']) && $filter['country_id'] ? " and users.country_id =".$filter['country_id']: "";
        $query = "select * from (
            select *,@row_number:=@row_number+1 AS player_rank from (
                select 
                users.*,
                quiz_answers.user_id,
                SUM(score) as total_score,
                quiz_answers.user_id = 1 as is_current
                from courses
                join lessons
                on lessons.course_id = courses.id
                join quizzes
                on quizzes.lesson_id = lessons.id
                join quiz_answers
                on quiz_answers.quiz_id = quizzes.id 
                join users
                on users.id = quiz_answers.user_id
                where courses.id = {$id} {$countryQuery}
                group by quiz_answers.user_id
                order by SUM(score) DESC, is_current DESC
            ) as groupedQuizParticipant
        ) as rankedQuizParticipant";

        // Get Top
        \DB::statement(\DB::raw('set @row_number=0'));
        $top = \DB::select(\DB::raw("select * from ({$query}) topTable limit 3"));
        // Get Bottom
        \DB::statement(\DB::raw('set @row_number=0'));
        $bottom = \DB::select(\DB::raw("select * from ({$query}) topTable order by player_rank DESC, is_current DESC limit 3"));

        \DB::statement(\DB::raw('set @row_number=0'));
        $user = collect(\DB::select(\DB::raw("select * from ({$query}) topTable where user_id = {$userId}")))->first();

        \DB::statement(\DB::raw('set @row_number=0'));
        $surroundingRanks = [$user->player_rank-1,$user->player_rank,$user->player_rank+1];
        $surrounding = \DB::select(\DB::raw("select * from ({$query}) topTable where player_rank in (".implode(',',$surroundingRanks).")"));
        
        $participants = collect($top)
                            ->merge(collect($bottom))
                            ->merge(collect($surrounding))
                            ->unique('user_id')
                            ->sortBy('player_rank')
                            ->map(function ($participant){
                                return [
                                    'user' => [
                                        "id" => $participant->id,
                                        "name" => $participant->name,
                                        "email" => $participant->email,
                                        "email_verified_at" => $participant->email_verified_at,
                                        "password" => $participant->password,
                                        "remember_token" => $participant->remember_token,
                                        "country_id" => $participant->country_id,
                                        "created_at" => $participant->created_at,
                                        "updated_at" => $participant->updated_at,
                                    ],
                                    'user_id' => $participant->user_id,
                                    'total_score' => $participant->total_score,
                                    'is_current' => $participant->is_current,
                                    'rank' => $participant->player_rank,
                                ];
                            })
                            ->values()
                            ->toArray();
        
        return [
            'user' => collect($participants)->filter(function ($user) use ($userId) {
                return $user['user_id'] == $userId;
            })->first(),
            'participants' => $participants
        ];
    }
}