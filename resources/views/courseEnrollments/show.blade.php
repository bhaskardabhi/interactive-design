<?php
/**
 * @var \App\CourseEnrollment $enrollment
 */
?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <h2 class="card-header">Lessons</h2>
                    <div class="card-body">
                        <ol>
                            @foreach($enrollment->course->lessons as $lesson)
                                <li>
                                    <a href="{{ route('lessons.show', ['slug' => $enrollment->course->slug, 'number' => $lesson->number]) }}">
                                        {{ $lesson->title }}
                                    </a>
                                </li>
                            @endforeach
                        </ol>
                    </div>
                </div>

                <div class="card mt-4">
                    <h2 class="card-header">Statistics</h2>
                    <div class="card-body">
                        <p>
                            Your rankings improve every time you answer a question correctly.
                            Keep learning and earning course points to become one of our top learners!
                        </p>
                        <div class="row">
                            <div class="col-md-6">
                                <h4>You are ranked <b>@th($leaderboard['country_wise']['user']['rank'])</b> in {{ auth()->user()->country->name }}</h4>
                                <ul style="padding: 0px;">
                                    @foreach($leaderboard['country_wise']['participants'] as $index => $participant)
                                        @php
                                            $scoreDiff = ($participant['total_score'] - $leaderboard['country_wise']['user']['total_score'])
                                        @endphp
                                        @php
                                            $next = isset($leaderboard['country_wise']['participants'][$index+1]) ? $leaderboard['country_wise']['participants'][$index+1] : null
                                        @endphp
                                        <li class="courseRanking__rankItem" style="display: flex; flex-direction: row; padding: 10px;">
                                            <div class="position" style="font-size: 28px; color: rgb(132, 132, 132); text-align: right; width: 80px; padding-right: 10px;">
                                                {{$participant['rank']}}
                                            </div>
                                            <div class="info">
                                                <div style="font-size: 16px;">
                                                    @if($participant['is_current'])
                                                        {{$participant['user']['name']}}
                                                    @else
                                                        <b>{{$participant['user']['name']}}</b>
                                                    @endif
                                                </div>
                                                <div class="score" style="font-size: 10px; color: rgb(132, 132, 132);">
                                                    {{$participant['total_score']}} PTS {{$scoreDiff > 0 ? "(+".$scoreDiff.")": ""}}
                                                </div>
                                            </div>
                                        </li>
                                        @if($next && $next['rank']-1 != $participant['rank'])
                                            <hr />
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                            <div class="col-md-6">
                                <h4>You are ranked <b>@th($leaderboard['global']['user']['rank'])</b> Worldwide</h4>
                                <ul style="padding: 0px;">
                                    @foreach($leaderboard['global']['participants'] as $index => $participant)
                                        @php
                                            $scoreDiff = ($participant['total_score'] - $leaderboard['global']['user']['total_score'])
                                        @endphp
                                        @php
                                            $next = isset($leaderboard['global']['participants'][$index+1]) ? $leaderboard['global']['participants'][$index+1] : null
                                        @endphp
                                        <li class="courseRanking__rankItem" style="display: flex; flex-direction: row; padding: 10px;">
                                            <div class="position" style="font-size: 28px; color: rgb(132, 132, 132); text-align: right; width: 80px; padding-right: 10px;">
                                                @if($participant['is_current'])
                                                    <b>{{$participant['rank']}}</b>
                                                @else
                                                    {{$participant['rank']}}
                                                @endif
                                            </div>
                                            <div class="info">
                                                <div style="font-size: 16px;">
                                                    @if($participant['is_current'])
                                                        <b>{{$participant['user']['name']}}</b>
                                                    @else
                                                        {{$participant['user']['name']}}
                                                    @endif
                                                </div>
                                                <div class="score" style="font-size: 10px; color: rgb(132, 132, 132);">
                                                    @if($participant['is_current'])
                                                        <b>{{$participant['total_score']}} PTS {{$scoreDiff > 0 ? "(+".$scoreDiff.")": ""}}</b>
                                                    @else
                                                        {{$participant['total_score']}} PTS {{$scoreDiff > 0 ? "(+".$scoreDiff.")": ""}}
                                                    @endif
                                                    
                                                </div>
                                            </div>
                                        </li>
                                        @if($next && $next['rank']-1 != $participant['rank'])
                                            <hr />
                                        @endif
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection